const express = require("express"),
  debug = require("debug")("api"),
  router = express.Router(),
  uuid = require('uuid');
const uploadExif = require('upload-exif');

router.get("/pictures", (req, res) => {
  const rows = req.app.get("db").prepare("SELECT * from pic").all();
  res.json({data: rows});
});

router.post("/picture", uploadExif.uploadWithExifAndThumbnail('file'), (req, res) => {
  const hasExif = (req.file.exif) ? 1 : 0;
  const dateTime = req.file.exif.DateTimeOriginal || Date.now() / 1000; // exif uses seconds, not mills
  debug('Your file upload: ', req.file);
  const buff = Buffer.from(req.file.thumbnail, 'base64');
  const entry = [ uuid.v1(), dateTime, Buffer.from(buff, 'base64'), req.file.mimetype, req.file.size, hasExif ];
  const query = "INSERT INTO pic (id, date, thumbnail, mime, size, hasExif) VALUES (?,?,?,?,?,?)";
  const data = req.app.get("db").prepare(query).run(entry);
  res.json({ data: data});
});

router.delete("/picture/:id", (req, res) => {
  debug('delete for ', req.params.id);
  const data = req.app.get("db").prepare("DELETE FROM pic WHERE id = ?").run([req.params.id]);
  res.json({data: data});
});


module.exports = router;
